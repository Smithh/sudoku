#Omgyoloswag plz work
#never trust globals
import pygame
from random import *
import time
pygame.init()
pygame.font.init()
font = pygame.font.Font(None, 50)
font2 = pygame.font.Font(None, 30)
#vajalikud muutujad
x = 700
y = 700
size = (900,700)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Sudoku")
kontroll = [[""]*9, [""]*9, [""]*9, [""]*9, [""]*9, [""]*9, [""]*9, [""]*9, [""]*9]
#võtab pildid ja märgib need ära
nr1 =pygame.image.load("1.png")
nr2 =pygame.image.load("2.png")
nr3 =pygame.image.load("3.png")
nr4 =pygame.image.load("4.png")
nr5 =pygame.image.load("5.png")
nr6 =pygame.image.load("6.png")
nr7 =pygame.image.load("7.png")
nr8 =pygame.image.load("8.png")
nr9 =pygame.image.load("9.png")
nr0 = pygame.image.load("0.png")
piltx = 4
pilty = 4
#nuppudele väärtused
keyboard_keys = {}
for i in range(0,10):
    keyboard_keys["pygame.K_" + str(i)] = "nr" + str(i)
#kui teha uus mäng v restart siis hakkab kahte listi jublama, kerge fix et jääks üks korralik
def tabel_fix(tabel,kontroll):
    lugejax = 0
    lugejay = 0
    for i in kontroll:
        for y in i:
            if y == "":
                kontroll[lugejax][lugejay] = tabel[lugejax][lugejay]
            lugejay += 1
        lugejax += 1
        lugejay = 0
    return(kontroll)
#KONTROLLIKS VAJALIKUD Funktsioonid-------------
def sudoku_kontroll(osa): #kontrollin numbrite 1-9 sisalduvust
    if set(osa) == {1,2,3,4,5,6,7,8,9}:
        return True
    else:
        return False
def rida_korras(tabel, rida):#kontrollin rea numbriga rida

    return sudoku_kontroll(tabel[rida])

def veerg_korras(tabel, veerg):#kontrollin veeru numbriga veerg

    veerud = []
    for i in range(len(tabel)):
        veerud.append(tabel[i][veerg])
    return sudoku_kontroll(veerud)

def ruut_korras(tabel, rida, veerg):#kontrollin ruudu, mis algab asukohaga rida ja veerg

    ruut = []
    for i in range(len(tabel)//3):
        for j in range(len(tabel)//3):
            ruut.append(tabel[rida + i][veerg + j])
    return sudoku_kontroll(ruut)

def kontroll_sudoku(tabel):
    #kontrollin kas sudoku fail on tühi
    if tabel == [] or tabel == "":
        return False
    else:
        vastus = True
        #vead loen ülesse
        vead = {"rida":set(), "veerg":set(), "ruut":set()}
        #kontrollin kõik läbi, kui viga vahetan vastuse False-ks ja lisan kirjutan vea ülesse
        for i in range(len(tabel)):
            if not rida_korras(tabel, i):
                vastus = False
                vead["rida"].add(i)
                return False

            if not veerg_korras(tabel, i):
                vastus = False
                vead["veerg"].add(i)
                return False

        for e in range(0,len(tabel),3):
            for j in range(0,len(tabel),3):
                if not ruut_korras(tabel, e, j):
                    vastus = False
                    vead["ruut"].add((e, j))
                    return False
    if vastus:
        return True
#--------------------------------------------------
#valib hunnikute väljade seast mida lahendada
def valija(x,map,restart_k,restart):
    mapid = x.readlines()
    map1 = []
    lugeja = 0
    for i in mapid:
        map1.append(i.strip("\n").split(" "))
    if restart_k == 0:
        x = randrange(0,9)
        restart = x
    else:
        x = restart
    while lugeja != 9:
        map.append(map1[lugeja+(x*9)])
        lugeja += 1
    return map,restart

#Märgib ära kohad kus on juba ette antud numbrid et teada kuhu ei tohi lasta kirjutada
def nono(map,kinni):
    global nrveel
    onumnrid = []
    järgx = 1
    järgy = 1
    kasutamine = []
    for i in map:
        for x in i:
            for a in x:
                if a != "0":
                    kasutamine.append(((700/9)*järgx)-(700/9))
                    kasutamine.append((700/9)*järgx)
                    kasutamine.append(((700/9)*järgy)-(700/9))
                    kasutamine.append((700/9)*järgy)
                    kinni.append(kasutamine)
                    onumnrid.append([eval("nr"+str(a)), (700/9)*järgx-(700/9)+piltx, (700/9)*järgy-(700/9)+pilty])
                    nrveel -= 1
                    #list kuhu loeb numbrid, mitte 0
                kontroll[järgy-1][järgx-1] = int(a)
                järgx += 1
                kasutamine = []
        järgy += 1
        järgx = 1
    tabel = kontroll
    return kinni,onumnrid,tabel
#kontroll mingi hetk et ei lase seda asukohta salvestada
def poskontroll(x,y,lõpukontroll,kinni):
    for i in kinni:
        if lõpukontroll == 0:
            if x> i[0] and x < i[1] and y > i[2] and y< i[3] or x > 700:
                return False
        #kui õigesti lahendatud on siis ei lase enam midagi muuta
        if lõpukontroll == 1:
            if x> i[0] and x < i[1] and y > i[2] and y< i[3] or x > 700 or x < 700:
                return False
    return True


def ääred(x,y):
    #ülevalt alla
    pygame.draw.line(screen,(0,0,255),(x/9,0),(x/9,y),2)
    pygame.draw.line(screen,(0,0,255),((x/9)*2,0),((x/9)*2,y),2)
    pygame.draw.line(screen,(100,0,255),((x/9)*3,0),((x/9)*3,y),4)
    pygame.draw.line(screen,(0,0,255),((x/9)*4,0),((x/9)*4,y),2)
    pygame.draw.line(screen,(0,0,255),((x/9)*5,0),((x/9)*5,y),2)
    pygame.draw.line(screen,(100,0,255),((x/9)*6,0),((x/9)*6,y),4)
    pygame.draw.line(screen,(0,0,255),((x/9)*7,0),((x/9)*7,y),2)
    pygame.draw.line(screen,(0,0,255),((x/9)*8,0),((x/9)*8,y),2)
    pygame.draw.line(screen,(0,0,255),((x/9)*9,0),((x/9)*9,y),2)
    #vasakult paremale
    pygame.draw.line(screen,(0,0,255),(0,y/9),(x,y/9),2)
    pygame.draw.line(screen,(0,0,255),(0,(y/9)*2),(x,(y/9)*2),2)
    pygame.draw.line(screen,(100,0,255),(0,(y/9)*3),(x,(y/9)*3),4)
    pygame.draw.line(screen,(0,0,255),(0,(y/9)*4),(x,(y/9)*4),2)
    pygame.draw.line(screen,(0,0,255),(0,(y/9)*5),(x,(y/9)*5),2)
    pygame.draw.line(screen,(100,0,255),(0,(y/9)*6),(x,(y/9)*6),4)
    pygame.draw.line(screen,(0,0,255),(0,(y/9)*7),(x,(y/9)*7),2)
    pygame.draw.line(screen,(0,0,255),(0,(y/9)*8),(x,(y/9)*8),2)

#leiab kordinaadid et joonistada aktiivne lahter
def aktiivne(a,b,x,kordinaatx,kordinaaty):
    üks = 700/9
    kordinaatx += 100
    kordinaaty += 100
    while kordinaatx < a:
        kordinaatx += üks
    kordinaatx -= üks
    while kordinaaty < b:
        kordinaaty += üks
    kordinaaty -= üks

    return kordinaatx,kordinaaty

#Programmi põhiosa
def start_game(font,font2,x,y,kordinaatx,kordinaaty,s,ss,m,h,lõpukontroll,algus,map,kinni,onumnrid,kontroll,ruudud,keyboard_keys,piltx,pilty,uus_mäng,uuesti_käivitamine,mäng_läbi,restart_k,restart,tabel,restart_b):
    done = False
    global nrveel
    nrveel = 81
    text = (str(h)+ " : " + str(m)+ " : " +str(s))
    map,restart = valija(open("map.txt"),map,restart_k,restart)
    kinni,onumnrid,tabel = nono(map,kinni)
    while not done:
        #KÕIK EVENTID
        for evt in pygame.event.get():
            if evt.type == pygame.QUIT:
                done = True
            if evt.type == pygame.KEYDOWN:
                if evt.key  == pygame.K_DELETE or evt.key == pygame.K_BACKSPACE:
                    klahv = pygame.K_0
                else:
                    klahv = evt.key
                for keys in keyboard_keys:
                    if klahv == eval(keys) and kordinaatx >= 0 and kordinaaty >= 0:
                        indeks = -1
                        for vr in ruudud:
                            if kordinaatx+piltx == vr[1] and kordinaaty+pilty == vr[2]:
                                indeks = ruudud.index(vr)
                        if indeks >= 0:
                            ruudud[indeks] = [eval(keyboard_keys[keys]), kordinaatx+piltx, kordinaaty+pilty]
                            keynr = keyboard_keys[keys].split("nr")
                            if int(keynr[1]) == 0 and kontroll[int(kordinaaty/77.7777777)][int(kordinaatx/77.7777777)] != 0:
                                nrveel += 1
                            elif int(keynr[1]) != 0 and kontroll[int(kordinaaty/77.7777777)][int(kordinaatx/77.7777777)] == 0:
                                nrveel -= 1
                            kontroll[int(kordinaaty/77.7777777)][int(kordinaatx/77.7777777)] = int(keynr[1])
                            break
                        else:
                            ruudud.append([eval(keyboard_keys[keys]), kordinaatx+piltx, kordinaaty+pilty])
                            keynr = keyboard_keys[keys].split("nr")
                            kontroll[int(kordinaaty/77.7777777)][int(kordinaatx/77.7777777)] = int(keynr[1])
                            nrveel -= 1 #mitu arvu jäänud
                            break

            if evt.type == pygame.MOUSEBUTTONDOWN:
                kordinaatx = -100
                kordinaaty = -100
                a,b = evt.pos
                #NEW-GAME------------------------------------------
                if a > uus_mäng[0] and a < uus_mäng[1] and b > uus_mäng[2] and b < uus_mäng[3]:
                    start_game(font,font2,x,y,-100,-100,0,0,0,0,0,0,[],[],[],[[""]*9, [""]*9, [""]*9, [""]*9, [""]*9, [""]*9, [""]*9, [""]*9, [""]*9],[],keyboard_keys,4,4,[715,879,500,540],0,False,0,0,tabel,[715,880,570,610])
                    uuesti_käivitamine = 1
                #RESTART-----------------------------------
                if a > restart_b[0] and a < restart_b[1] and b > restart_b[2] and b < restart_b[3]:
                    start_game(font,font2,x,y,-100,-100,s,ss,m,h,0,0,[],[],[],[[""]*9, [""]*9, [""]*9, [""]*9, [""]*9, [""]*9, [""]*9, [""]*9, [""]*9],[],keyboard_keys,4,4,[715,879,500,540],0,False,1,restart,tabel,[715,880,570,610])
                    uuesti_käivitamine = 1
                if poskontroll(a,b,lõpukontroll,kinni) == True:
                    kordinaatx,kordinaaty = aktiivne(a,b,x,kordinaatx,kordinaaty)
        #KÕIK MIS EI OLE EVENTID

        #ARVUTAMISED----------
        if uuesti_käivitamine == 1:
            break
        screen.fill((255,255,255))
        ääred(x,y)
        #TIMER----------------------------
        us = time.localtime(time.time())[5]
        if mäng_läbi != True:
            if ss != us:
                if s < 60:
                    s+= 1
                if s == 60:
                    m += 1
                    s = 0
                if m == 60:
                    h+= 1
                    m = 0
                text = (str(h)+ " : " + str(m)+ " : " +str(s))
            ss = us
        #----------------

        #JOONISTAMISED -----------------
        aeg = font.render(text,5,(0,0,0),(255,255,255 ))
        screen.blit(aeg, (725, 80))
        tekst = font.render("Aeg",5,(0,0,0),(255,255,255 ))
        screen.blit(tekst,(725,40))
        teataja2 = font2.render("Õige Lahendus",5,(0,0,0),(255,255,255))
        uus = font.render("Uus Mäng",5,(255,0,0),(255,255,255))
        screen.blit(uus,(715,500))
        restart_button = font.render("Restart",5,(255,0,0),(255,255,255))
        screen.blit(restart_button,(715,570))
        #kastikesed nuppude ümber
        pygame.draw.rect(screen,(0,0,255),(710,495,175,45),4)
        pygame.draw.rect(screen,(0,0,255),(710,565,175,45),4)

        if nrveel <= 0:
            teataja = font.render("Viga",5,(0,0,0),(255,255,255))
            #kontroll
            if (kontroll_sudoku(tabel_fix(tabel,kontroll))) == True:
                mäng_läbi = True
                screen.blit(teataja2,(725,150))
                pygame.draw.rect(screen,(0,255,0),(0,0,700,700),8)
                lõpukontroll = 1
            else:
                screen.blit(teataja,(725,150))
        if nrveel > 0:
            teataja = font.render("",5,(0,0,0),(255,255,255))
            screen.blit(teataja,(725,150))
        for info in onumnrid:
            screen.blit(info[0], (info[1], info[2]))
        for info in ruudud:
            screen.blit(info[0], (info[1], info[2]))
        pygame.draw.rect(screen,(255,255,0),(kordinaatx,kordinaaty,700/9,700/9),8)
        #PROGRAMMI PÕHIRIDA, Everything else is thrash!--------
        pygame.display.flip()
        #--------------------------------


start_game(font,font2,x,y,-100,-100,0,0,0,0,0,0,[],[],[],[[""]*9, [""]*9, [""]*9, [""]*9, [""]*9, [""]*9, [""]*9, [""]*9, [""]*9],[],keyboard_keys,4,4,[715,880,500,540],0,False,0,0,[],[715,880,570,610])